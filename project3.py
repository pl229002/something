from flask import Flask
from roboclass import MotorDriver
from flask import jsonify, request


app = Flask(__name__)
motor = MotorDriver()


# @app.route("/forward/<int:inchesCount>", methods=['GET'])
@app.route('/forward/<int:inchesCount>')
def forward(inchesCount):
   motor.MotorForward(inchesCount)
   return jsonify({'forward': inchesCount, 'success': True})

@app.route("/reverse/<int:inchesCount>")
def reverse(inchesCount):
   motor.MotorReverse(inchesCount)
   return "Going back"
   return jsonify({'reverse': inchesCount, 'success': True})


@app.route("/left/<int:turnCount>")
def left(turnCount):
   motor.MotorLeft(turnCount)
   return "Going left"
   return jsonify({'left': inchesCount, 'success': True})


@app.route("/right/<int:turnCount>")
def right(turnCount):
   motor.MotorRight(turnCount)
   return jsonify({'right': inchesCount, 'success': True})


@app.route("/stop/")
def stop():
   motor.MotorStop()
   return jsonify({'stop': inchesCount, 'success': True})

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=5000)  # run our Flask app
